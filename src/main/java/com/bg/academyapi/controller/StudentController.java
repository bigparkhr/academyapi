package com.bg.academyapi.controller;

import com.bg.academyapi.model.StudentRequest;
import com.bg.academyapi.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor //누군가가 꼭 필요.
@RequestMapping ("/student")
public class StudentController {
    private final StudentService studentService;

    @PostMapping("/new")
    public String setstudent(@RequestBody StudentRequest request) {
        studentService.setStudent(request);
        return "OK";
    }
}